<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Tanggal Registrasi</label>
						<input type="text" readonly name="user_terdaftar" class="form-control"  value="<?= date('d-m-Y')?>">
					</div>				
					<div class="form-group">
						<label>Nama Lengkap*</label>
						<input type="text" required name="user_nama" class="text-capitalize form-control" title="Wajib diisi">
					</div>
					<div class="form-group">
						<label>Username*</label>
						<input type="text" required name="user_user" class="form-control" title="Wajib diisi">
					</div>
					<div class="form-group">
						<label>Password*</label>
						<input type="password" required name="user_password" class="form-control" title="Wajib diisi">
					</div>
					<div class="form-group">
						<label>Password*</label>
						<select type="text" name="user_status" style="width:100%" class="select2 form-control">
							<option value="1">Admin</option>
							<option value="2">User</option>
						</select>
					</div>															
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-primary">Simpan</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>