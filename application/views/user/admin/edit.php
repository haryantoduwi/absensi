<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Id</label>
						<input type="text" readonly name="id" class="form-control"  value="<?= $data->user_id?>">
					</div>				
					<div class="form-group">
						<label>Tanggal Registrasi</label>
						<input type="text" readonly name="user_terdaftar" class="form-control"  value="<?= date('d-m-Y',strtotime($data->user_terdaftar))?>">
					</div>				
					<div class="form-group">
						<label>Nama Lengkap*</label>
						<input type="text" required name="user_nama" class="text-capitalize form-control" title="Wajib diisi" value="<?= $data->user_nama?>">
					</div>
					<div class="form-group">
						<label>Username*</label>
						<input type="text" required name="user_user" class="form-control" title="Wajib diisi" value="<?= $data->user_user?>">
					</div>
					<div class="form-group">
						<label>Password*</label>
						<input type="password" name="user_password" class="form-control" title="Wajib diisi">
						<p class="help-block">Biarkan kosong jika tidak dirubah</p>
					</div>
					<div class="form-group">
						<label>Password*</label>
						<select type="text" name="user_status" style="width:100%" class="select2 form-control">
							<option value="1" <?= $data->user_status==1 ? 'selected':''?>>Admin</option>
							<option value="2" <?= $data->user_status==2 ? 'selected':''?>>User</option>
						</select>
					</div>															
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-primary">Update</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>