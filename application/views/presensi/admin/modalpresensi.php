<div class="modal-dialog modal-md">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title"><?= 'Siswa :'.ucwords($data->siswa_nama)?></h4>
    </div>
    <div class="modal-body">
      <form id="formadd" readonly action="<?= base_url($global->url)?>" method="POST">
        <div class="form-group">
          <label>Id Siswa</label>
          <input type="text" name="presensi_idsiswa" class="form-control" value="<?= $data->siswa_id?>">
        </div>
         <div class="form-group">
           <button type="submit" name="hadir" value="1"  data-dismiss="modal" class="btn btn-flat btn-block btn-success">Hadir</button>
           <button type="submit" name="ijin"  value="1"  data-dismiss="modal" class="btn btn-flat btn-block btn-primary">Ijin</button>
           <button type="submit" name="alpha" value="1"  data-dismiss="modal" class="btn btn-flat btn-block btn-danger">Alpha</button>
         </div>
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn pull-right btn-flat btn-default" data-dismiss="modal">Tutup</button>
    </div>
  </div>
</div>
<?php include 'action.js'; ?>