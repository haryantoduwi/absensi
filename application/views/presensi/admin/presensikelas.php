<table style="width:100%" id="tabelbiasa" class="table table-bordered table-striped">
    <thead>
        <tr>
          <th width="5%">No</th>
          <th width="45%">Nama</th>
          <th class="hidden-xs" width="20%">Nis</th>
          <th class="hidden-xs" width="15%">Kelas</th>
          <th width="15%" class="text-center">Keterangan</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1;foreach ($data as $row):?>
            <tr>
                <td><?=$i?></td>
                <td><a href="#" class="presensinama" url="<?= base_url($global->url.'presensinama')?>" id="<?=$row->siswa_id?>"><?=ucwords($row->siswa_nama)?></a></td>
                <td class="hidden-xs"><?=ucwords($row->siswa_nis)?></td>
                <td class="hidden-xs"><?=ucwords($row->kelas_kelas)?></td>
                <td>
                   
                </td>
            </tr>                       
        <?php $i++;endforeach;?>
    </tbody>                    
</table>
<p>Keterengan : <br>
    <a href="#" class="btn btn-flat btn-xs btn-warning" style="width:25px"><span class="fa fa-eye"></span></a> : Detail<br>
    <a href="#" class="btn btn-flat btn-xs btn-info" style="width:25px"><span class="fa fa-pencil"></span></a> : Edit<br>
    <a href="#" class="btn btn-flat btn-xs btn-danger" style="width:25px"><span class="fa fa-trash"></span></a> : Hapus 
</p>
<?php include 'action.js'; ?>