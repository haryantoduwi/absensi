<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="row">
						<div class="col-sm-2">
							<div class="form-group">
								<label>Tanggal Presensi</label>
								<input type="text" readonly class="form-control"  value="<?=date('d-m-Y')?>">
							</div>									
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>Kelas</label>
								<select type="text" id="idkelas" name="siswa_idkelas" style="width:100%" class="select2 form-control">
									<?php foreach($kelas AS $row):?>
										<option value="<?= $row->kelas_id?>"><?= ucwords($row->kelas_kelas)?></option>
									<?php endforeach;?>
								</select>
							</div>								
						</div>
						<div class="col-sm-2">
							<div class="form-group">
								<label>&nbsp</label>
								<button type="button" id="pilihkelas" url="<?= base_url($global->url.'presensikelas')?>" class="btn btn-block btn-flat btn-primary">Pilih</button>
							</div>							
						</div>
					</div>
					<hr>
					<div class="row">
						<div class="col-sm-12">
							<div id="tampildata" class="table-responsive">
								
							</div>
						</div>
					</div>																	
				</form>			
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-presensi"></div>
<?php include 'action.js';?>