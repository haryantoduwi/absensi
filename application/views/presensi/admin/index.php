<div id="form">
	
</div>
<div class="row mainview">
	<div class="col-sm-2">
		<div class="form-group">
			<button id="add" url="<?= base_url($global->url.'add')?>" class="btn btn-flat btn-block btn-primary"><span class="fa fa-plus"></span> Tambah Data</button>
		</div>
	</div>
	<div class="col-sm-12">
		<div id="tabel" class="table-responsive">
			<div class="text-center">
				<p><span class="fa fa-spin fa-refresh"></span>	Loading...</p>			
			</div>		
		</div>

	</div>
</div>
<!--DETAIL DATA-->
<div id="edit" class="modal fade">
</div>
<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){
			$("#tabel").load("<?php echo base_url($global->url.'tabel')?>");		
		},1000)
	    $('#add').click(function(){
	      var url=$(this).attr('url');
	      $("#form").load(url);
	      $(".mainview").hide();       
	    })      		
	})
</script>