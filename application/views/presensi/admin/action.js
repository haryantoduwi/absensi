<script type="text/javascript">
	$(document).ready(function(){
		action();
		datatabel();
	    $('.datepicker').datepicker({
	      autoclose: true,
	      todayHighlight: true,
	      format: "dd-mm-yyyy",
	      todayBtn: true,
	    });     
	    $('#tabelbiasa').DataTable();
	    //Initialize Select2 Elements
	    $(".select2").select2(); 
	    $(".pricetag").priceFormat({
	        prefix:'Rp ',
	        thousandsSeparator:'.',
	        centsLimit:'0'
	    }) 		
	})
	function action(){
	    $('#formadd').on('submit',function(e){
	        e.preventDefault();
	        //alert('hello');
	        $.ajax({
	          type:$(this).attr('method'),
	          url:$(this).attr('action'),
	          data:$(this).serialize(),
	          success:function(){
	     //      	$('#modal-presensi').modal('hide',{backdrop:'false'});
	     //       	$("#form").load("<?= base_url($global->url.'presensikelas')?>");
		  		// $(".mainview").hide();           
	          }
	        })
	    });		
		$('#pilihkelas').click(function(){
		  var url=$(this).attr('url');
		  var id=$('[name=siswa_idkelas]').val();
		  //alert(id);
		  $.ajax({
		    type:'POST',
		    url:url,
		    data:{id:id},
		    success:function(data){
		      $('#tampildata').html(data);
		      //$(".selectdata").select2(); 
		    }
		  })
		  return false;
		})	
		$('.presensinama').click(function(){
		  var url=$(this).attr('url');
		  var id=$(this).attr('id');
		  //alert(id);
		  $.ajax({
		    type:'POST',
		    url:url,
		    data:{id:id},
		    success:function(data){
		      $('#modal-presensi').html(data);
		      $('#modal-presensi').modal('show',{backdrop:'true'});
		      //$(".selectdata").select2(); 
		    }
		  })
		  return false;
		})			
		$('.detail').click(function(){
		  var url=$(this).attr('url');
		  var id=$(this).attr('id');
		  //alert(id);
		  $.ajax({
		    type:'POST',
		    url:url,
		    data:{id:id},
		    success:function(data){
		      $('#detail').html(data);
		      $('#detail').modal('show',{backdrop:'true'});
		      //$(".selectdata").select2(); 
		    }
		  })
		  return false;
		})
		$('.edit').click(function(){
		  var url=$(this).attr('url');
		  var id=$(this).attr('id');
		  //alert(url);
		  $.ajax({
		    type:'POST',
		    url:url,
		    data:{id:id},
		    success:function(data){
		      $('#form').html(data);
		      $(".mainview").hide();        
		    }
		  })
		  return false;
		})    
		$('.hapus').click(function(){
		  var url=$(this).attr('url');
		  swal({
		    title:'Perhatian',
		    text:'Hapus Data',
		    html:true,
		    ConfirmButtonColor:'#d9534F',
		    showCancelButton:true,
		    type:'warning'
		  },function(){
		    window.location.href=url
		  });
		  return false
		})    
		$('#add').click(function(){
		  var url=$(this).attr('url');
		  $("#form").load(url);
		  $(".mainview").hide();       
		})    
	}
	function datatabel(){
		//Data Tabel 
		$('#datatabel').DataTable({
		  "destroy": true,
		  "paging": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false,
		    dom: 'Bfrtip',
		    buttons: [
		        {
		          extend:'excelHtml5',
		          title:'Daftar User',
		          exportOptions:{
		            columns:[0,1,2,3]
		          }
		        },		        
		        {
		          extend:'pdfHtml5',
		          title:'Daftar User',
		          exportOptions:{
		            columns:[0,1,2,3]
		          }
		        },             
		        {
		          extend:'print',
		          title:'Daftar User',
		          exportOptions:{
		            columns:[0,1,2,3]
		          }
		        }            
		    ]     
		});
		$(".dt-button").addClass('btn btn-flat btn-sm btn-primary');
		$(".dt-button.btn").css('margin','2px');  
	}		
</script>