<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Id Kelas</label>
						<input type="text" readonly name="id" class="form-control"  value="<?=$data->kelas_id?>">
					</div>				
					<div class="form-group">
						<label>Tanggal Disimpan</label>
						<input type="text" readonly name="kelas_tersimpan" class="form-control"  value="<?= date('d-m-Y',strtotime($data->kelas_tersimpan))?>">
					</div>				
					<div class="form-group">
						<label>Nama Kelas*</label>
						<input type="text" required name="kelas_kelas" class="text-capitalize form-control" title="Wajib diisi" value="<?=ucwords($data->kelas_kelas)?>">
					</div>
					<div class="form-group">
						<label>Wali Kelas*</label>
						<input type="text" required name="kelas_wali" class="form-control" title="Wajib diisi" value="<?=ucwords($data->kelas_wali)?>">
					</div>
					<div class="form-group">
						<label>Keterangan </label>
						<textarea class="text-capitalize form-control" name="kelas_keterangan" rows="4"><?= $data->kelas_keterangan?></textarea>
					</div>														
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-warning">Simpan</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>