<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Id</label>
						<input type="text" readonly name="id" class="form-control"  value="<?= $data->semester_id?>">
					</div>				
					<div class="form-group">
						<label>Semester</label>
						<input type="text" required name="semester_nama" class="text-capitalize form-control" title="Wajib diisi"  value="<?= $data->semester_nama?>">
					</div>
					<div class="form-group">
						<label>Password*</label>
						<select type="text" name="semester_status" style="width:100%" class="select2 form-control">
							<option value="1" <?= $data->semester_status==1? 'selected':''?>>Aktif</option>
							<option value="0" <?= $data->semester_status==0? 'selected':''?>>Non Aktif</option>
						</select>
					</div>															
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-warning">Simpan</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>