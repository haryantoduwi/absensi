<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Id</label>
						<input type="text" name="id" readonly class="form-control"  value="<?= $data->siswa_id?>">
					</div>				
					<div class="form-group">
						<label>Nis</label>
						<input type="text" required name="siswa_nis" class="text-capitalize form-control" title="Wajib diisi" value="<?= $data->siswa_nis?>">
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" required name="siswa_nama" class="text-capitalize form-control" title="Wajib diisi" value="<?= $data->siswa_nama?>">
					</div>	

					<div class="form-group">
						<label>Kelas</label>
						<select type="text" name="siswa_idkelas" style="width:100%" class="select2 form-control">
							<?php foreach($kelas AS $row):?>
								<option value="<?= $row->kelas_id?>" <?= $data->siswa_idkelas==$row->kelas_id ? 'selected':''?>><?= ucwords($row->kelas_kelas)?></option>
							<?php endforeach;?>
						</select>
					</div>															
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-warning">Simpan</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>