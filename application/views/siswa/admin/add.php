<div class="row">
	<div class="col-sm-12 animated fadeInRight">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title"><?= ucwords($global->headline)?></h3>
			</div>
			<div class="box-body">
				<form method="POST" action="<?= base_url($global->url)?>">
					<div class="form-group">
						<label>Id</label>
						<input type="text" readonly class="form-control"  value="Auto Generated">
					</div>				
					<div class="form-group">
						<label>Nis</label>
						<input type="text" required name="siswa_nis" class="text-capitalize form-control" title="Wajib diisi">
					</div>
					<div class="form-group">
						<label>Nama</label>
						<input type="text" required name="siswa_nama" class="text-capitalize form-control" title="Wajib diisi">
					</div>	

					<div class="form-group">
						<label>Kelas</label>
						<select type="text" name="siswa_idkelas" style="width:100%" class="select2 form-control">
							<?php foreach($kelas AS $row):?>
								<option value="<?= $row->kelas_id?>"><?= ucwords($row->kelas_kelas)?></option>
							<?php endforeach;?>
						</select>
					</div>															
					<div class="form-group">
						<button type="submit" value="submit" name="submit" class="btn btn-block btn-flat btn-primary">Simpan</button>
					</div>
				</form>			
			</div>
		</div>
	</div>
</div>
<?php include 'action.js';?>