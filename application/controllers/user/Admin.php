<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('Crud');	//LOAD MODEL
		if(($this->session->userdata('login')!=true) || ($this->session->userdata('level')!=1) ){
			redirect(site_url('login/logout'));
		}
		$this->id=$this->session->userdata('user_id');
		$this->level=$this->session->userdata('level');
	}
	private $master_tabel="user";				//TABEL MASTER
	private $default_url="user/Admin";			//PATH URL CONTROLLER
	private $default_view="user/admin/";			//FOLDER VIEW 
	private $view="template/backend";

	private function global_set($data){	//PARAMETER GLOBAL UNTUK VIEW NANTI
		$data=array(
			'menu'=>'user',								//MENU 
			'submenu'=>false,							//SUBMENU AKTIF JIKA MEMILIKI SUBMENU
			'headline'=>$data['headline'],				//TEXT UNTUK DITAMPILAN MENU
			'url'=>$data['url'],						//URL YANG DIAKSES UNTUK DITAMPILKAN DI VIEW
			'ikon'=>"fa fa-users",						//IKON MENU 	
			//PARAMETER AKSI
			'level'=>$this->level,
			'detail'=>false,
			'delete'=>true,
			'edit'=>true,

			'base_uri'=>"user/",						//PATH UNTUK GLOBAL CONTROLLER BACKEND
			'view'=>"views/user/admin/index.php",				//PATH UNTUK VIEW
		);
		return (object)$data;
	}	
	private function notifiaksi($param){ //FUNGSI UNTUK MENAMPILKAN NOTIFIKASI
		if($param==1){
			$this->session->set_flashdata('success','proses berhasil');
		}else{
			$this->session->set_flashdata('error',$param);
		}		
	}
	public function index()
	{
		$global_set=array(
			'submenu'=>false,
			'headline'=>'User',
			'url'=>'user/Admin/',
		);
		$global=$this->global_set($global_set);
		if($this->input->post('submit')){
			$data=array(
				'user_nama'=>$this->input->post('user_nama'),
				'user_user'=>$this->input->post('user_user'),
				'user_password'=>md5($this->input->post('user_password')),
				'user_terdaftar'=>date('Y-m-d',strtotime($this->input->post('user_terdaftar'))),
				'user_status'=>$this->input->post('user_status'),
				//'user_foto'=>$this->input->post('registrasi_negara'),												
			);
			$query=array(
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->insert($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'tabel'=>$this->master_tabel,
				'order'=>array('kolom'=>$this->id,'orderby'=>'ASC'),
				);
			$data=array(
				'global'=>$global,
				'data'=>$this->Crud->read($query)->result(),
			);
			$this->load->view($this->view,$data);
			//print_r($data['grafik']);
		}
	}
	public function tabel(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'User',
			'url'=>'user/Admin/',
		);
		$global=$this->global_set($global_set);		
		$query=array(
			'tabel'=>$this->master_tabel,
			'order'=>array('kolom'=>$this->id,'orderby'=>'ASC'),
			);
		$data=array(
			'global'=>$global,
			'data'=>$this->Crud->read($query)->result(),
		);
		$this->load->view($this->default_view.'tabel',$data);		
	}
	public function hapus($id){
		$query=array(
			'where'=>array('user_id'=>$id),
			'tabel'=>$this->master_tabel,
		);
		$delete=$this->Crud->delete($query);
		$this->notifiaksi($delete);
		redirect(site_url($this->default_url));
	}
	public function add(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'user',
			'url'=>'user/Admin',
		);
		$global=$this->global_set($global_set);
		$data=array(
			'global'=>$global,
			);		
		$this->load->view($this->default_view.'add',$data);
	}
	public function edit(){
		$global_set=array(
			'submenu'=>false,
			'headline'=>'edit user',
			'url'=>'user/Admin/edit',
		);
		$global=$this->global_set($global_set);
		$id=$this->input->post('id');
		if($this->input->post('submit')){
			$data=array(
				'user_nama'=>$this->input->post('user_nama'),
				'user_user'=>$this->input->post('user_user'),
				'user_terdaftar'=>date('Y-m-d',strtotime($this->input->post('user_terdaftar'))),
				'user_status'=>$this->input->post('user_status'),
				//'user_foto'=>$this->input->post('registrasi_negara'),												
			);
			if($this->input->post('user_password')){
				$data['user_password']=md5($this->input->post('user_password'));
			}
			$query=array(
				'where'=>array('user_id'=>$id),
				'data'=>$data,
				'tabel'=>$this->master_tabel,
			);
			$insert=$this->Crud->update($query);
			$this->notifiaksi($insert);
			redirect(site_url($this->default_url));
		}else{
			$query=array(
				'where'=>array('user_id'=>$id),
				'tabel'=>$this->master_tabel,
			);
			$data=array(
				'global'=>$global,
				'data'=>$this->Crud->read($query)->row(),	
				);		
			$this->load->view($this->default_view.'edit',$data);
		}
	}	
}
