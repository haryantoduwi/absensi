/*
Navicat MySQL Data Transfer

Source Server         : mysq on localhost
Source Server Version : 50611
Source Host           : localhost:3306
Source Database       : absensi_db

Target Server Type    : MYSQL
Target Server Version : 50611
File Encoding         : 65001

Date: 2018-08-17 15:44:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for kelas
-- ----------------------------
DROP TABLE IF EXISTS `kelas`;
CREATE TABLE `kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_kelas` varchar(255) DEFAULT NULL,
  `kelas_keterangan` text,
  `kelas_wali` varchar(255) DEFAULT NULL,
  `kelas_tersimpan` date DEFAULT NULL,
  PRIMARY KEY (`kelas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of kelas
-- ----------------------------
INSERT INTO `kelas` VALUES ('1', 'I A', 'Kelas Satu A, kapasitas siswa 35 orang', 'Mark Marquest', '2018-08-16');
INSERT INTO `kelas` VALUES ('3', '1 B', 'kelas satu b, kapasitas siswa 35 orang', 'Mark Marquest', '2018-08-16');

-- ----------------------------
-- Table structure for presensi
-- ----------------------------
DROP TABLE IF EXISTS `presensi`;
CREATE TABLE `presensi` (
  `presensi_id` int(11) NOT NULL AUTO_INCREMENT,
  `presensi_idsiswa` int(11) DEFAULT NULL,
  `presensi_idsemester` int(11) NOT NULL,
  `presensi_tgl` datetime DEFAULT NULL,
  `presensi_hadir` varchar(10) DEFAULT NULL,
  `presensi_sakit` varchar(10) DEFAULT NULL,
  `presensi_alpha` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`presensi_id`),
  KEY `fk_absen_idsiswa` (`presensi_idsiswa`),
  KEY `fk_absen_idsemester` (`presensi_idsemester`),
  CONSTRAINT `fk_absen_idsemester` FOREIGN KEY (`presensi_idsemester`) REFERENCES `semester` (`semester_id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_absen_idsiswa` FOREIGN KEY (`presensi_idsiswa`) REFERENCES `siswa` (`siswa_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of presensi
-- ----------------------------

-- ----------------------------
-- Table structure for semester
-- ----------------------------
DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `semester_id` int(11) NOT NULL AUTO_INCREMENT,
  `semester_nama` varchar(255) DEFAULT NULL,
  `semester_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`semester_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of semester
-- ----------------------------
INSERT INTO `semester` VALUES ('1', 'Semester 1', '1');
INSERT INTO `semester` VALUES ('3', 'Semester 2', '0');

-- ----------------------------
-- Table structure for siswa
-- ----------------------------
DROP TABLE IF EXISTS `siswa`;
CREATE TABLE `siswa` (
  `siswa_id` int(11) NOT NULL AUTO_INCREMENT,
  `siswa_nis` varchar(20) DEFAULT NULL,
  `siswa_nama` varchar(255) DEFAULT NULL,
  `siswa_idkelas` int(11) DEFAULT NULL,
  PRIMARY KEY (`siswa_id`),
  KEY `fk_siswa_idkelas` (`siswa_idkelas`),
  CONSTRAINT `fk_siswa_idkelas` FOREIGN KEY (`siswa_idkelas`) REFERENCES `kelas` (`kelas_id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of siswa
-- ----------------------------
INSERT INTO `siswa` VALUES ('1', '231313131', 'duwi haryantooooo', '1');
INSERT INTO `siswa` VALUES ('2', '12321321421', 'Ariana Grande', '1');
INSERT INTO `siswa` VALUES ('3', '8989089890', 'radhitya mahakarya', '1');
INSERT INTO `siswa` VALUES ('4', '211412', 'gandhi prayogo', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_nama` varchar(255) DEFAULT NULL,
  `user_user` varchar(255) DEFAULT NULL,
  `user_password` text,
  `user_terdaftar` date DEFAULT NULL,
  `user_status` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Duwi', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2018-08-15', '1');
INSERT INTO `user` VALUES ('3', 'eko nosariyanto', 'gembul', '385cde1f7728cf48205309eabbdf31fc', '2018-08-15', '2');
INSERT INTO `user` VALUES ('5', 'mahatma gandi', 'gandi', 'a0c308fa53555d573a38809fb3cba564', '2018-08-16', '2');
